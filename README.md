## Painty
Simple Paint clone build with vanilla JS


## Installation
- Clone the repository


## Usage
Open index.html in the browser.


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.


## Version
MVP


## License 
MIT