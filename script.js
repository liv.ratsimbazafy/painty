const activeToolEl = document.getElementById("active-tool");
const brushColorBtn = document.getElementById("brush-color");
const brushIcon = document.getElementById("brush");
const brushSize = document.getElementById("brush-size");
const brushSlider = document.getElementById("brush-slider");
const bucketColorBtn = document.getElementById("bucket-color");
const eraser = document.getElementById("eraser");
const clearCanvasBtn = document.getElementById("clear-canvas");
const saveStorageBtn = document.getElementById("save-storage");
const loadStorageBtn = document.getElementById("load-storage");
const clearStorageBtn = document.getElementById("clear-storage");
const downloadBtn = document.getElementById("download");
const { body } = document;

const canvas = document.createElement("canvas");
canvas.id = "canvas";
const context = canvas.getContext("2d");

let currentSize = 10;
let bucketColor = "#FFFFFF";
let currentColor = "#A51DAB";

let isEraser = false;
let isMouseDown = false;
let drawnArray = [];

function displayBrushSize() {
    if (brushSlider.value < 10) brushSize.textContent = "0" + brushSlider.value;
    else brushSize.textContent = brushSlider.value;
}

// Brush
brushSlider.addEventListener("change", () => {
    currentSize = brushSlider.value;
    displayBrushSize();
});

brushColorBtn.addEventListener("change", () => {
    isEraser = false;
    currentColor = `#${brushColorBtn.value}`;
});

// Bucket
bucketColorBtn.addEventListener("change", () => {
    bucketColor = `#${bucketColorBtn.value}`;
    createCanvas();
    restoreCanvas();
});

// Eraser
eraser.addEventListener("click", () => {
    isEraser = true;
    brushIcon.style.color = "blueViolet";
    brushIcon.style.background = "aliceBlue";
    eraser.style.color = "aliceBlue";
    eraser.style.background = "blueViolet";
    activeToolEl.textContent = "Eraser";
    currentColor = bucketColor;
    currentSize = 50;
});

function switchToBrush() {
    isEraser = false;
    activeToolEl.textContent = "Brush";
    brushIcon.style.color = "aliceBlue";
    brushIcon.style.background = "blueViolet";
    eraser.style.color = "blueViolet";
    eraser.style.background = "aliceBlue";

    currentColor = `#${brushColorBtn.value}`;
    currentSize = 10;
    brushSlider.value = 10;
    displayBrushSize();
}

//Canvas operations
function createCanvas() {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight - 80;
    context.fillStyle = bucketColor;
    context.fillRect(0, 0, canvas.width, canvas.height);
    body.appendChild(canvas);
    switchToBrush();
}

clearCanvasBtn.addEventListener("click", () => {
    createCanvas();
    drawnArray = [];
    activeToolEl.textContent = "Canvas Cleared";
    setTimeout(switchToBrush, 2500);
});

function restoreCanvas() {
    for (let i = 1; i < drawnArray.length; i++) {
        context.beginPath();
        context.moveTo(drawnArray[i - 1].x, drawnArray[i - 1].y);
        context.lineWidth = drawnArray[i].size;
        context.lineCap = "round";
        if (drawnArray[i].eraser) context.strokeStyle = bucketColor;
        else context.strokeStyle = drawnArray[i].color;

        context.lineTo(drawnArray[i].x, drawnArray[i].y);
        context.stroke();
    }
}

function storeDrawn(x, y, size, color, erase) {
    const line = {
        x,
        y,
        size,
        color,
        erase,
    };
    drawnArray.push(line);
}

// Mouse Position
function getMousePosition(e) {
    const boundaries = canvas.getBoundingClientRect();
    return {
        x: e.clientX - boundaries.left,
        y: e.clientY - boundaries.top,
    };
}

// Mouse Events
canvas.addEventListener("mousedown", (e) => {
    isMouseDown = true;
    const currentPosition = getMousePosition(e);
    context.moveTo(currentPosition.x, currentPosition.y);
    context.beginPath();
    context.lineWidth = currentSize;
    context.lineCap = "round";
    context.strokeStyle = currentColor;
});

canvas.addEventListener("mousemove", (event) => {
    if (isMouseDown) {
        const currentPosition = getMousePosition(event);
        context.lineTo(currentPosition.x, currentPosition.y);
        context.stroke();
        storeDrawn(
            currentPosition.x,
            currentPosition.y,
            currentSize,
            currentColor,
            isEraser
        );
    } else {
        storeDrawn(undefined);
    }
});

canvas.addEventListener("mouseup", () => (isMouseDown = false));

// Storage operations
saveStorageBtn.addEventListener("click", () => {
    localStorage.setItem("my_canvas", JSON.stringify(drawnArray));
    activeToolEl.textContent = "Canvas Saved";
    setTimeout(switchToBrush, 2500);
});

loadStorageBtn.addEventListener("click", () => {
    let myCanvas = localStorage.getItem("my_canvas");

    if (myCanvas) {
        drawnArray = JSON.parse(localStorage.getItem("my_canvas"));
        restoreCanvas(drawnArray);
        activeToolEl.textContent = "Canvas Loaded";
        setTimeout(switchToBrush, 2500);
    } else {
        activeToolEl.textContent = "No canvas found";
        setTimeout(switchToBrush, 2500);
    }
});

clearStorageBtn.addEventListener("click", () => {
    localStorage.removeItem("my_canvas");
    activeToolEl.textContent = "Local Storage Cleared";
    setTimeout(switchToBrush, 2500);
});

// Download operation
downloadBtn.addEventListener("click", () => {
    downloadBtn.href = canvas.toDataURL("image/jpeg", 1);
    downloadBtn.download = "my_paint.jpeg";
    activeToolEl.textContent = "Image File Saved";
    setTimeout(switchToBrush, 1500);
});

brushIcon.addEventListener("click", switchToBrush);

createCanvas();
